#include<stdio.h>
#include<sys/types.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<signal.h>

#define MAX_INPUT_SIZE 1024
#define MAX_TOKEN_SIZE 64
#define MAX_NUM_TOKENS 64

/* Splits the string by space and returns the array of tokens
*
*/
char **tokenize(char *line) {
  char **tokens = (char **)malloc(MAX_NUM_TOKENS * sizeof(char *));
  char *token = (char *)malloc(MAX_TOKEN_SIZE * sizeof(char));
  int tokenIndex = 0, tokenNo = 0;
  unsigned long i;

  for(i = 0; i < strlen(line); i++){

    char readChar = line[i];

    if (readChar == ' ' || readChar == '\n' || readChar == '\t'){
      token[tokenIndex] = '\0';
      if (tokenIndex != 0){
	tokens[tokenNo] = (char*)malloc(MAX_TOKEN_SIZE*sizeof(char));
	strcpy(tokens[tokenNo++], token);
	tokenIndex = 0; 
      }
    } else {
      token[tokenIndex++] = readChar;
    }
  }
 
  free(token);
  tokens[tokenNo] = NULL ;
  return tokens;
}

/* Implementation of cd in order to change directories
*
*/
int change_dir(char** tokens) {
    char buf[MAX_INPUT_SIZE];

	char *get_dir = getcwd(buf, sizeof(buf));
    char *current_dir = strcat(get_dir, "/");
    char *ending_dir = strcat(current_dir, tokens[1]); // TODO: fix this

	chdir(ending_dir);

	return 0;
}

/* Implementation of exit to leave shell 
*
*/
void exit_process(char** tokens) {
	if (tokens[1] != NULL) {
		printf("too many args try again");
	} else {
		exit(0); //Successful exit
	}
}

void handle_cmd(char **cmd_tokens, char *special) {
	if (cmd_tokens[0] == NULL) {
		printf("Please enter command.");
	} else if (strcmp(cmd_tokens[0], "exit") == 0) {
		// exit shell
		exit_process(cmd_tokens);
	} else if (strcmp(cmd_tokens[0], "cd") == 0) {
		// change dir
		change_dir(cmd_tokens);
	} else {
		int retval = fork();
		if (retval == 0) {
			// child process
			execvp(cmd_tokens[0], cmd_tokens);
			perror("ERROR: Could not execute %s. Incorrect command, please try again. \n");
			exit(1);
		} else {
			// parent process
			int pid = retval;
			wait(&pid);
		}

		if (special != NULL) {
			printf("special char is: %s \n", special);
		}
	}
}

void process_tokens(char **tokens) {
	char *special = NULL;
	int i, j;
	for (i = 0; tokens[i] != NULL; i++) {
		char **cmd_tokens = (char **)malloc(MAX_NUM_TOKENS * sizeof(char *));
		for (j = 0; tokens[i] != NULL; i++, j++){
			if (strcmp(tokens[i], "&") == 0 || strcmp(tokens[i], "&&") == 0 || strcmp(tokens[i], "&&&") == 0) {
				special = tokens[i];
				i++;
				break;
			}
			cmd_tokens[j] = tokens[i];
		}
		cmd_tokens[j] = NULL;
		handle_cmd(cmd_tokens, special);
		free(cmd_tokens);
	}
}

/* Main function to test shell
*
*/
int main(int argc, char *argv[]) {
	char  line[MAX_INPUT_SIZE];            
	char  **tokens;              
	int i;

	FILE *fp; //changed this to refactor
	if(argc == 2) {
		fp = fopen(argv[1],"r");
		if(fp < 0) {
			perror("File does not exist.");
			return -1;
		}
	}

	while(1) {		
		/********* BEGIN: TAKING INPUT *********/
		bzero(line, sizeof(line));
		if(argc == 2) { // batch mode
			if(fgets(line, sizeof(line), fp) == NULL) { // file reading finished
				break;	
			}
			line[strlen(line) - 1] = '\0';
		} else { // interactive mode
			printf("$ ");
			scanf("%[^\n]", line);
			getchar();
		}
		// printf("Command entered: %s (remove this debug output later)\n", line);
		/********* END: TAKING INPUT *********/
		/********* BEGIN: Tokenize INPUT *********/
		line[strlen(line)] = '\n'; //terminate with new line
		tokens = tokenize(line);
		/********* END: Tokenize INPUT *********/


		if (*tokens != NULL) {
			process_tokens(tokens);
		} else {
			perror("Nothing entered.");
		}

		/********* BEGIN: handle memory *********/
		for (i = 0; tokens[i] != NULL; i++){
			free(tokens[i]);
		}
		free(tokens); // Freeing the allocated memory	
		/********* END: handle memory *********/
	}
	return 0;
}
